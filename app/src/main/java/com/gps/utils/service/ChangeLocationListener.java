package com.gps.utils.service;


import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import com.gps.utils.store.Storage;

import java.util.TimerTask;

public class ChangeLocationListener implements LocationListener {

    private Storage storage;

    public ChangeLocationListener(Storage storage) {
        this.storage = storage;
    }

    @Override
    public void onLocationChanged(Location location) {
        storage.storeLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}