package com.gps.utils.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.gps.utils.network.TrackingApiClient;
import com.gps.utils.store.Storage;

import java.util.TimerTask;


public class ConnectTask extends TimerTask {

    private final String deviceId;
    private Storage storage;

    private TrackingApiClient trackingApiClient;
    private Context context;

    public ConnectTask(Storage storage, TrackingApiClient trackingApiClient, String deviceId, Context context) {
        this.storage = storage;
        this.trackingApiClient = trackingApiClient;
        this.deviceId = deviceId;
        this.context = context;
    }

    @Override
    public void run() {
        if(isInternetConnected(context) && trackingApiClient.recordCrumbs(storage.getAllSavedLocations(), deviceId)) {
            storage.clearStore();
        }
    }

    private boolean isInternetConnected (Context ctx) {
        ConnectivityManager connectivityMgr = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi != null) {
            if (wifi.isConnected()) {
                return true;
            }
        }
        if (mobile != null) {
            if (mobile.isConnected()) {
                return true;
            }
        }
        return false;
    }
}