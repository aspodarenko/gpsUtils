package com.gps.utils.service;

import android.app.Service;
import android.content.Intent;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.IBinder;
import android.provider.Settings;
import com.gps.utils.GpsUtilsApplication;
import com.gps.utils.components.DaggerGpsUtilsApplicationComponent;
import com.gps.utils.moduls.GpsUtilsModule;
import com.gps.utils.network.TrackingApiClient;
import com.gps.utils.store.Storage;

import javax.inject.Inject;
import java.util.Timer;


public class GPSCordinateTranslatorService extends Service {

    private static final long RECORD_LOCATION_PERIOD = 30000L;
    private static final long TRACKING_API_UPDATE_PERIOD = 60000L;


    private Storage storage;

    private TrackingApiClient client;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        startForeground();
        DaggerGpsUtilsApplicationComponent.builder().gpsUtilsModule(new GpsUtilsModule((GpsUtilsApplication) this.getApplication())).build().inject(this);
        ChangeLocationListener listener = new ChangeLocationListener(storage);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        ((LocationManager) this.getBaseContext().getSystemService(LOCATION_SERVICE)).requestLocationUpdates(RECORD_LOCATION_PERIOD, 0, criteria, listener, null);

        Timer trackingApiUpdateTimer = new Timer();
        String deviceId = Settings.Secure.getString(getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        trackingApiUpdateTimer.schedule(new ConnectTask(storage, client, deviceId, this.getBaseContext()), 0, TRACKING_API_UPDATE_PERIOD);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public Storage getStorage() {
        return storage;
    }

    @Inject
    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public TrackingApiClient getClient() {
        return client;
    }

    @Inject
    public void setClient(TrackingApiClient client) {
        this.client = client;
    }
}
