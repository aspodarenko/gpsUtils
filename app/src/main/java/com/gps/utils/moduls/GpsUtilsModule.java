package com.gps.utils.moduls;

import android.content.Context;
import android.util.Log;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.SecurityUtils;
import com.gps.utils.GpsUtilsApplication;
import com.gps.utils.network.TrackingApiClient;
import com.gps.utils.store.JsonStorage;
import com.gps.utils.store.Storage;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.util.Arrays;

@Module
public class GpsUtilsModule {

    private static final String EMAIL_ID = "1025563533698-q3285m666hk9pq6ljma0psbe2p0ta179@developer.gserviceaccount.com";

    private static final String SECRET_KEY = "ae0a90133513.p12";

    private static final String TRACKS_SCOPE = "https://www.googleapis.com/auth/tracks";

    private static final HttpTransport httpTransport = new NetHttpTransport();

    private static final JsonFactory json_factory = new JacksonFactory();
    private static final String STORE_PASS = "notasecret";
    private static final String ALIAS = "privatekey";
    private static final String KEY_PASS = "notasecret";

    private GpsUtilsApplication application;

    public GpsUtilsModule() {

    }

    public GpsUtilsModule(GpsUtilsApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    public HttpRequestFactory provideHttpRequestFactory() {
        PrivateKey privateKey = null;
        try {
            privateKey = SecurityUtils.loadPrivateKeyFromKeyStore(SecurityUtils.getPkcs12KeyStore(), application.getBaseContext().getAssets().open(SECRET_KEY), STORE_PASS, ALIAS, KEY_PASS);
        } catch (IOException e) {
            Log.e(provideApplicationName(), e.getMessage());
        } catch (GeneralSecurityException e) {
            Log.e(provideApplicationName(), e.getMessage());
        }
        GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(json_factory).setServiceAccountId(EMAIL_ID)
                .setServiceAccountScopes(Arrays.asList(TRACKS_SCOPE)).setServiceAccountPrivateKey(privateKey).build();
        return httpTransport.createRequestFactory(credential);

    }

    @Provides
    @Singleton
    public String provideApplicationName() {
        if(application != null) {
            int stringId = application.getBaseContext().getApplicationInfo().labelRes;
            return application.getBaseContext().getString(stringId);
        } else {
            return "unknown";
        }
    }

    @Provides
    @Singleton
    public Storage provideStore(){
        return new JsonStorage(application.getBaseContext());
    }

    @Provides
    @Singleton
    public TrackingApiClient provideTrackingApiClient(){
        return new TrackingApiClient(application);
    }


}
