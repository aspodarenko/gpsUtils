package com.gps.utils.moduls;


import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;
import java.io.IOException;
@Module
public class MockInjectionModule {

    @Provides
    @Singleton
    public HttpRequestFactory provideHttpRequestFactory(){
        return new NetHttpTransport().createRequestFactory();
    }

    @Provides
    @Singleton
    public HttpRequest provideHttpRequest() {
        try {
            return  this.provideHttpRequestFactory().buildPostRequest(null,null);
        } catch (IOException e) {
            return null;
        }
    }


    @Provides
    @Singleton
    public String applicationName(){
        return "appName";
    }
}
