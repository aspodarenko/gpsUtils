package com.gps.utils.receiver;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.gps.utils.service.GPSCordinateTranslatorService;

public abstract class AbstractGPSUtilsBroadcastReceiver extends BroadcastReceiver {

    protected boolean isServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    protected void runCoordinateTranslatorServiceIfNeeded(Context context){
        if (!isServiceRunning(GPSCordinateTranslatorService.class, context)) {
            Intent serviceIntent = new Intent(context, GPSCordinateTranslatorService.class);
            context.startService(serviceIntent);
        }
    }
}
