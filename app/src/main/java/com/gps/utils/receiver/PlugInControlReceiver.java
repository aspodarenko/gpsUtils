package com.gps.utils.receiver;

import android.content.Context;
import android.content.Intent;
import com.gps.utils.service.GPSCordinateTranslatorService;

public class PlugInControlReceiver extends AbstractGPSUtilsBroadcastReceiver {

    @Override
    public void onReceive(Context context , Intent intent) {
        String action = intent.getAction();
        if(action.equals(Intent.ACTION_POWER_CONNECTED)) {
            runCoordinateTranslatorServiceIfNeeded(context);
        }
    }
}
