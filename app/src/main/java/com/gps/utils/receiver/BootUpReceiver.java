package com.gps.utils.receiver;


import android.content.Context;
import android.content.Intent;
import com.gps.utils.service.GPSCordinateTranslatorService;

public class BootUpReceiver extends AbstractGPSUtilsBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
           runCoordinateTranslatorServiceIfNeeded(context);
        }
    }
}
