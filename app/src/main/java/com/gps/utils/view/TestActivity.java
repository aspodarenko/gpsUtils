package com.gps.utils.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.gps.utils.R;
import com.gps.utils.service.GPSCordinateTranslatorService;

public class TestActivity extends Activity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);
    }

    @Override
    public void onClick(View v) {
            Intent serviceIntent = new Intent(getBaseContext(), GPSCordinateTranslatorService.class);
            getBaseContext().startService(serviceIntent);
    }


}