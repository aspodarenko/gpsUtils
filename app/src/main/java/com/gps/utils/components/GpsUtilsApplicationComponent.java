package com.gps.utils.components;

import android.app.Application;
import com.gps.utils.moduls.GpsUtilsModule;
import com.gps.utils.moduls.MockInjectionModule;
import com.gps.utils.network.TrackingApiClient;
import com.gps.utils.service.GPSCordinateTranslatorService;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = { GpsUtilsModule.class} )
public interface GpsUtilsApplicationComponent {
    public void inject(Application application);
    public void inject(TrackingApiClient application);
    public void inject(GPSCordinateTranslatorService gpsCordinateTranslatorService);
}
