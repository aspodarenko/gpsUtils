package com.gps.utils.components;

import com.gps.utils.moduls.GpsUtilsModule;
import com.gps.utils.moduls.MockInjectionModule;
import com.gps.utils.network.TrackingApiClient;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = { MockInjectionModule.class} )
public interface MockComponent {
    public void inject(TrackingApiClient application);
}
