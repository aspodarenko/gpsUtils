package com.gps.utils.network;


import android.util.Log;
import com.google.api.client.http.*;
import com.google.gson.Gson;
import com.gps.utils.GpsUtilsApplication;
import com.gps.utils.components.DaggerGpsUtilsApplicationComponent;
import com.gps.utils.dto.*;
import com.gps.utils.components.GpsUtilsApplicationComponent;
import com.gps.utils.moduls.GpsUtilsModule;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class TrackingApiClient {
    public static final String APPLICATION_JSON = "application/json";

    private static final GenericUrl RECORD_CRUMB_METHOD_URL = new GenericUrl("https://www.googleapis.com/tracks/v1/crumbs/record");

    private static final GenericUrl ENTITY_CREATE_METHOD_URL = new GenericUrl("https://www.googleapis.com/tracks/v1/entities/create");

    private static final GenericUrl ENTITY_GET_LIST_METHOD_URL = new GenericUrl("https://www.googleapis.com/tracks/v1/entities/list");

    private static final Integer MAX_CRUMB_IN_ONE_REQUEST = 1024;
    private static final String SUCCESSFUL_RECORD_CRUMBS_RESPONSE = "{}\n";

    private GpsUtilsApplicationComponent applicationComponent;

    private String applicationName;

    private Gson gson = new Gson();


    public TrackingApiClient(GpsUtilsApplication application) {
        applicationComponent = DaggerGpsUtilsApplicationComponent.builder().gpsUtilsModule(new GpsUtilsModule(application)).build();
    }

    private HttpRequestFactory requestFactory;

    public List<String> createEntities(List<String> names) {
        List<String> result = new LinkedList<>();
        String content = gson.toJson(buildCreateEntityRequest(names));
        try {
            HttpRequest request = getRequestFactory().buildPostRequest(ENTITY_CREATE_METHOD_URL, ByteArrayContent.fromString(null, content));
            EntityCreateResponse response = gson.fromJson(sendRequest(request), EntityCreateResponse.class);
            result = response.getEntityIds();
        } catch (IOException e) {
            Log.e(getApplicationName(), "error while sending create entity request" + content + " " + e.toString());
        }
        return result;
    }

    public Boolean recordCrumbs(List<Crumb> crumbs, String name) {
        String entityId = getEntityId(name);
        if (entityId == null) {
            entityId = createEntities(Arrays.asList(name)).get(0);
        }
        Boolean result = true;
        Integer positionOfLastAlreadySentElement = 0;
        while (positionOfLastAlreadySentElement < crumbs.size()) {
            Integer elementsLeftToSend = crumbs.size() - positionOfLastAlreadySentElement;
            Integer endElementIndex;
            if (elementsLeftToSend >= MAX_CRUMB_IN_ONE_REQUEST) {
                endElementIndex = positionOfLastAlreadySentElement + MAX_CRUMB_IN_ONE_REQUEST;
            } else {
                endElementIndex = positionOfLastAlreadySentElement + elementsLeftToSend;
            }
            List<Crumb> listToSend = crumbs.subList(positionOfLastAlreadySentElement, endElementIndex);
            if (!recordCrumbsInternal(listToSend, entityId)) {
                result = false;
            }
            positionOfLastAlreadySentElement = endElementIndex;
        }
        return result;
    }

    private String getEntityId(String name) {
        HttpRequest request = null;
        try {
            request = getRequestFactory().buildPostRequest(ENTITY_GET_LIST_METHOD_URL, ByteArrayContent.fromString(null, "{}"));
            String response = sendRequest(request);
            if (!response.isEmpty()) {
                GetEntityListResponse getEntityListResponse = gson.fromJson(response, GetEntityListResponse.class);
                for (Entity entity : getEntityListResponse.getEntities()) {
                    if (name.equals(entity.getName())) {
                        return entity.getId();
                    }
                }
            }
            return null;
        } catch (IOException e) {
            Log.e(getApplicationName(), "error while getting entity list " + e.toString());
            return null;
        }
    }

    Boolean recordCrumbsInternal(List<Crumb> crumbs, String entityId) {
        String content = gson.toJson(buildRecordCrumbRequest(crumbs, entityId));
        Boolean result;
        try {
            HttpRequest request = getRequestFactory().buildPostRequest(RECORD_CRUMB_METHOD_URL, ByteArrayContent.fromString(null, content));
            String response = sendRequest(request);
            result = response.equals(SUCCESSFUL_RECORD_CRUMBS_RESPONSE);
        } catch (IOException e) {
            Log.e(getApplicationName(), "error while sending record crumbs request" + content + " " + e.toString());
            result = false;
        }
        return result;
    }

    EntityCreateRequest buildCreateEntityRequest(List<String> names) {
        EntityCreateRequest entityCreateRequest = new EntityCreateRequest();
        List<Entity> entities = new LinkedList<>();
        for (String name : names) {
            Entity entity = new Entity();
            entity.setName(name);
            entities.add(entity);
        }
        entityCreateRequest.setEntities(entities);
        return entityCreateRequest;
    }

    String sendRequest(HttpRequest request) throws IOException {
        request.getHeaders().setContentType(APPLICATION_JSON);
        return request.execute().parseAsString();
    }

    RecordCrumbRequest buildRecordCrumbRequest(List<Crumb> crumbs, String entityId) {
        RecordCrumbRequest request = new RecordCrumbRequest();
        request.setCrumbs(crumbs);
        request.setEntityId(entityId);
        return request;
    }


    public HttpRequestFactory getRequestFactory() {
        if (requestFactory == null) {
            applicationComponent.inject(this);
        }
        return requestFactory;
    }

    @Inject
    public void setRequestFactory(HttpRequestFactory requestFactory) {
        this.requestFactory = requestFactory;
    }


    public String getApplicationName() {
        if (applicationName == null) {
            applicationComponent.inject(this);
        }
        return applicationName;
    }

    @Inject
    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }


}
