package com.gps.utils.store;


import android.content.Context;
import android.location.Location;
import com.gps.utils.dto.Crumb;

import java.util.List;

public interface Storage {

    public void storeLocation(Location location);

    public List<Crumb> getAllSavedLocations();

    public void clearStore();
}
