package com.gps.utils.store;


import android.content.Context;
import android.location.Location;
import android.util.Log;
import com.google.gson.Gson;
import com.gps.utils.dto.Crumb;
import com.gps.utils.dto.RecordCrumbRequest;

import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class JsonStorage implements Storage {

    private Context context;

    public JsonStorage(Context context){
          this.context = context;
    }

    private Gson gson = new Gson();

    private static final String STORE_FILE_NAME = "crumbs";

    @Override
    public  void storeLocation(Location location) {
        com.gps.utils.dto.Location jsonLocation = new com.gps.utils.dto.Location();
        jsonLocation.setLat(String.valueOf(location.getLatitude()));
        jsonLocation.setLng(String.valueOf(location.getLongitude()));
        Crumb crumb = new Crumb();
        crumb.setLocation(jsonLocation);
        crumb.setTimestamp(location.getTime() / 1000);
        RecordCrumbRequest recordCrumbRequest = new RecordCrumbRequest();
        List<Crumb> crumbs = getAllSavedLocations();
        crumbs.add(crumb);
        recordCrumbRequest.setCrumbs(crumbs);
        try {
            synchronized (this) {
                FileOutputStream stream = context.openFileOutput(STORE_FILE_NAME, Context.MODE_PRIVATE);
                byte[] byteArray = gson.toJson(recordCrumbRequest).getBytes();
                stream.write(byteArray, 0, byteArray.length);
                stream.close();
            }
        } catch (FileNotFoundException e) {
            Log.e(context.getApplicationInfo().name, "File " + STORE_FILE_NAME + " not found"+ e.getMessage());
        } catch (IOException e) {
            Log.e(context.getApplicationInfo().name, "file output stream from json error" + e.getMessage());
        }

    }

    @Override
    public List<Crumb> getAllSavedLocations() {
        String readedCrumbs = readCrumbsFromFile();
        if(readedCrumbs.isEmpty()){
            return new LinkedList<>();
        }
        RecordCrumbRequest request = gson.fromJson(readedCrumbs, RecordCrumbRequest.class);
        return request.getCrumbs();
    }

    synchronized String readCrumbsFromFile(){
        StringBuffer stringBuffer = new StringBuffer();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(context.openFileInput(STORE_FILE_NAME)));
            String line = null;
            while((line = reader.readLine()) != null){
                stringBuffer.append(line);
            }
            reader.close();
            return stringBuffer.toString();
        } catch (IOException e) {
            Log.e(context.getApplicationInfo().name, "file input stream error" + e.getMessage());
            return "";
        }
    }

    @Override
    public synchronized void clearStore() {
        context.deleteFile(STORE_FILE_NAME);
    }
}
