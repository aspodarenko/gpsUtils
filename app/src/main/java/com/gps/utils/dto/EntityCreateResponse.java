package com.gps.utils.dto;


import java.util.List;

public class EntityCreateResponse {

    private List<String> entityIds;

    public List<String> getEntityIds() {
        return entityIds;
    }

    public void setEntityIds(List<String> entityIds) {
        this.entityIds = entityIds;
    }
}
