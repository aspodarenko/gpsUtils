package com.gps.utils.dto;

import java.util.List;

public class GetEntityListResponse {

    private List<Entity> entities;

    public List<Entity> getEntities() {
        return entities;
    }

    public void setEntities(List<Entity> entities) {
        this.entities = entities;
    }
}
