package com.gps.utils.dto;


import java.util.List;

public class RecordCrumbRequest {

    private List<Crumb> crumbs;

    private String entityId;

    public List<Crumb> getCrumbs() {
        return crumbs;
    }

    public void setCrumbs(List<Crumb> crumbs) {
        this.crumbs = crumbs;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
}
