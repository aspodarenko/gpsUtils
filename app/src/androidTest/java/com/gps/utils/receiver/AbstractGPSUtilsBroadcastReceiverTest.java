package com.gps.utils.receiver;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.gps.utils.service.GPSCordinateTranslatorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(RobolectricTestRunner.class)
public class AbstractGPSUtilsBroadcastReceiverTest {

    private AbstractGPSUtilsBroadcastReceiver receiver = new BootUpReceiver();

    private ActivityManager activityManager;

    private Context context;

    private ActivityManager.RunningServiceInfo runningServiceInfo1;

    private ActivityManager.RunningServiceInfo runningServiceInfo2;

    @Before
    public void setUp() {
        context = mock(Context.class);
        activityManager = mock(ActivityManager.class);
        when(context.getSystemService(Context.ACTIVITY_SERVICE)).thenReturn(activityManager);
        runningServiceInfo1 = mock(ActivityManager.RunningServiceInfo.class);
        runningServiceInfo1.service = mock(ComponentName.class);
        runningServiceInfo2 = mock(ActivityManager.RunningServiceInfo.class);
        runningServiceInfo2.service = mock(ComponentName.class);
        when(activityManager.getRunningServices(Integer.MAX_VALUE)).thenReturn(Arrays.asList(runningServiceInfo1, runningServiceInfo2));
    }

    @Test
    public void shouldNotRunServiceActivityManagerFoundServiceWithSameName() {
        when(runningServiceInfo1.service.getClassName()).thenReturn(GPSCordinateTranslatorService.class.getName());
        receiver.runCoordinateTranslatorServiceIfNeeded(context);
        verify(context,never()).startService(any(Intent.class));
    }

    @Test
    public void shouldRunServiceActivityManagerFoundServiceWithSameName() {
        receiver.runCoordinateTranslatorServiceIfNeeded(context);
        verify(context).startService(any(Intent.class));
    }


}