package com.gps.utils.service;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import com.gps.utils.store.Storage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class ChangeLocationListenerTest {

    private static String BEST_PROVIDER_ID = "providerId";

    private Storage storage;

    private Location location;

    private ChangeLocationListener listener;


    @Before
    public void setUp(){
        location = mock(Location.class);
        storage = mock(Storage.class);
        listener = new ChangeLocationListener(storage);
    }

    @Test
    public void shouldStoreLocationWhenItChanged(){
        listener.onLocationChanged(location);
        verify(storage).storeLocation(location);
    }

}