package com.gps.utils.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.gps.utils.dto.Crumb;
import com.gps.utils.network.TrackingApiClient;
import com.gps.utils.store.Storage;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class ConnectTaskTest {

    public static final String ENTITY_ID = "entityId";
    private ConnectTask connectTask;

    private TrackingApiClient client;

    private Storage storage;
    private Context context;
    private ConnectivityManager connectivityManager;

    private List<Crumb> crumbs = new LinkedList<>();
    private NetworkInfo wifi;
    private NetworkInfo mobile;

    @Before
    public void setUp() {
        storage = mock(Storage.class);
        client = mock(TrackingApiClient.class);
        context = mock(Context.class);
        connectivityManager = mock(ConnectivityManager.class);
        connectTask = new ConnectTask(storage,client,ENTITY_ID,context);
        when(context.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(connectivityManager);

        wifi = mock(NetworkInfo.class);
        when(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)).thenReturn(wifi);
        mobile = mock(NetworkInfo.class);
        when(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)).thenReturn(mobile);
    }

    @Test
    public void shouldSendRecordCrumbRequest() {
        when(wifi.isConnected()).thenReturn(true);
        when(storage.getAllSavedLocations()).thenReturn(crumbs);
        connectTask.run();
        verify(client).recordCrumbs(crumbs, ENTITY_ID);
    }

    @Test
    public void shouldGetCrumbsFromStore(){
        when(wifi.isConnected()).thenReturn(true);
        connectTask.run();
        verify(storage).getAllSavedLocations();
    }

    @Test
    public void shouldClearStoreIfCrumbsSendSuccessful(){
        when(wifi.isConnected()).thenReturn(true);
        when(storage.getAllSavedLocations()).thenReturn(crumbs);
        when(client.recordCrumbs(crumbs, ENTITY_ID)).thenReturn(true);
        connectTask.run();

        verify(storage).clearStore();
    }

    @Test
    public void shouldNotSendUpdateAndClearStoreIfNetworkIsDisconected(){
        when(wifi.isConnected()).thenReturn(false);
        when(mobile.isConnected()).thenReturn(false);
        connectTask.run();
        verify(client, never()).recordCrumbs(any(List.class), anyString());
        verify(storage, never()).clearStore();
    }

    @Test
    public void shouldSendUpdateAndClearStoreIfOnlyMobileInternetEnabled(){
        when(wifi.isConnected()).thenReturn(false);
        when(mobile.isConnected()).thenReturn(true);
        when(storage.getAllSavedLocations()).thenReturn(crumbs);
        when(client.recordCrumbs(crumbs, ENTITY_ID)).thenReturn(true);
        connectTask.run();
        verify(client).recordCrumbs(any(List.class), anyString());
        verify(storage).clearStore();
    }
}