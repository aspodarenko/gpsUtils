package com.gps.utils.network;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.gps.utils.components.DaggerMockComponent;
import com.gps.utils.components.MockComponent;
import com.gps.utils.dto.Crumb;
import com.gps.utils.dto.EntityCreateRequest;
import com.gps.utils.dto.RecordCrumbRequest;
import com.gps.utils.moduls.MockInjectionModule;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;


public class TrackingApiClientTest {
    private static final GenericUrl RECORD_CRUMB_METHOD_URL = new GenericUrl("https://www.googleapis.com/tracks/v1/crumbs/record");

    private static final GenericUrl ENTITY_CREATE_METHOD_URL = new GenericUrl("https://www.googleapis.com/tracks/v1/entities/create");

    private static final GenericUrl ENTITY_GET_LIST_METHOD_URL = new GenericUrl("https://www.googleapis.com/tracks/v1/entities/list");

    private static final String SUCCESSFUL_RECORD_CRUMBS_RESPONSE = "{}\n";

    private static final Integer MAX_CRUMB_IN_ONE_REQUEST = 1024;

    private static final String DEFAULT_ENTITY_CREATE_RESPONSE = "{\"entityIds\":[\"testNameId\",\"otherNameId\"]}";

    private static final String DEFAULT_ENTITY_GET_LIST_RESPONSE ="{\n" +
            "\"entities\": [\n" +
            "{" +
            "\"id\":\"entityId\"," +
            "\"name\": \"entityName\"" +
            "}" +
            "]" +
            "}";
    public static final String ENTITY_ID = "entityId";
    public static final String SOME_OTHER_ID = "someOtherId";
    public static final String ENTITY_NAME = "entityName";
    public static final String OTHER_ENTITY_NAME = "entityName2";

    private MockComponent mockAppComponent;

    private Crumb crumb = new Crumb();

    private TrackingApiClient originTrackingApiClient = new TrackingApiClient(null);

    private TrackingApiClient client;

    private List<Crumb> someBigCrumbList;


    @Before
    public void setUp() throws IOException {
        mockAppComponent = DaggerMockComponent.builder().mockInjectionModule(new MockInjectionModule()).build();
        mockAppComponent.inject(originTrackingApiClient);
        client = spy(originTrackingApiClient);

    }

    @Test
    public void shouldSendHttpRequest() throws IOException {
        doReturn(DEFAULT_ENTITY_CREATE_RESPONSE).when(client).sendRequest(any(HttpRequest.class));
        client.createEntities(Arrays.asList("testName"));
        verify(client).sendRequest(any(HttpRequest.class));
    }

    @Test
    public void shouldSetNamesForCreateEntityRequest() throws IOException {
        List<String> names = Arrays.asList("testName", "otherName");

        EntityCreateRequest request = client.buildCreateEntityRequest(names);

        assertEquals(names.size(),request.getEntities().size());
        for(Integer i = 0; i < names.size(); i++){
            assertEquals(request.getEntities().get(i).getName(),(names.get(i)));
        }
    }

    @Test
    public void shouldReturnListOfIdsWhenCreateEntityRequest() throws IOException {
        doReturn(DEFAULT_ENTITY_CREATE_RESPONSE).when(client).sendRequest(any(HttpRequest.class));

        List<String> resultIds = client.createEntities(Arrays.asList("testName","otherName"));

        assertEquals(resultIds.get(0),"testNameId");
        assertEquals(resultIds.get(1),"otherNameId");
    }

    @Test
    public void shouldAddUrlToRequestWhenCreateEntityRequest() throws IOException {
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        doReturn(DEFAULT_ENTITY_CREATE_RESPONSE).when(client).sendRequest(captor.capture());

        client.createEntities(Arrays.asList("testName"));
        assertEquals(ENTITY_CREATE_METHOD_URL,captor.getValue().getUrl());
    }

    @Test
    public void shouldSendRequestWithUrlWhenRecordCrumbs() throws IOException {
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        doReturn(DEFAULT_ENTITY_GET_LIST_RESPONSE).doReturn("").when(client).sendRequest(captor.capture());
        client.recordCrumbs(Arrays.asList(crumb),ENTITY_NAME);

        assertEquals(RECORD_CRUMB_METHOD_URL,captor.getValue().getUrl());
    }

    @Test
    public void shouldReturnTrueIfEmptyRecordCrumbResponse() throws IOException {
        doReturn(DEFAULT_ENTITY_GET_LIST_RESPONSE).doReturn(SUCCESSFUL_RECORD_CRUMBS_RESPONSE).when(client).sendRequest(any(HttpRequest.class));
        assertTrue(client.recordCrumbs(Arrays.asList(crumb),ENTITY_NAME));
    }

    @Test
    public void shouldCreateRecordCrumbsRequest(){
        List<Crumb> crumbs = Arrays.asList(crumb);
        RecordCrumbRequest request = client.buildRecordCrumbRequest(crumbs, ENTITY_ID);
        assertEquals(ENTITY_ID,request.getEntityId());
        assertEquals(crumbs,request.getCrumbs());
    }

    @Test
    public void shouldSplitBigListOfCrumbsToSeveralSmallAndSendItSeparately() throws IOException {
        doReturn(DEFAULT_ENTITY_GET_LIST_RESPONSE).when(client).sendRequest(any(HttpRequest.class));
        someBigCrumbList = mock(List.class);
        List<Crumb> firstPart = mock(List.class);
        List<Crumb> secondPart = mock(List.class);
        List<Crumb> thirdPart = mock(List.class);
        when(someBigCrumbList.size()).thenReturn(MAX_CRUMB_IN_ONE_REQUEST*3-2);
        when(someBigCrumbList.subList(0, MAX_CRUMB_IN_ONE_REQUEST)).thenReturn(firstPart);
        when(someBigCrumbList.subList(MAX_CRUMB_IN_ONE_REQUEST,MAX_CRUMB_IN_ONE_REQUEST*2)).thenReturn(secondPart);
        when(someBigCrumbList.subList(MAX_CRUMB_IN_ONE_REQUEST*2,MAX_CRUMB_IN_ONE_REQUEST*3-2)).thenReturn(thirdPart);
        doReturn(true).when(client).recordCrumbsInternal(any(List.class), eq(ENTITY_ID));

        assertTrue(client.recordCrumbs(someBigCrumbList,ENTITY_NAME));

        verify(client).recordCrumbsInternal(firstPart, ENTITY_ID);
        verify(client).recordCrumbsInternal(secondPart, ENTITY_ID);
        verify(client).recordCrumbsInternal(thirdPart, ENTITY_ID);
    }

    @Test
    public void shouldReturnFalseWhenAtLeastOneRequestFailure() throws IOException {
        doReturn(DEFAULT_ENTITY_GET_LIST_RESPONSE).when(client).sendRequest(any(HttpRequest.class));
        someBigCrumbList = mock(List.class);
        List<Crumb> firstPart = mock(List.class);
        List<Crumb> secondPart = mock(List.class);
        List<Crumb> thirdPart = mock(List.class);
        when(someBigCrumbList.size()).thenReturn(MAX_CRUMB_IN_ONE_REQUEST*3-2);
        when(someBigCrumbList.subList(0, MAX_CRUMB_IN_ONE_REQUEST)).thenReturn(firstPart);
        when(someBigCrumbList.subList(MAX_CRUMB_IN_ONE_REQUEST,MAX_CRUMB_IN_ONE_REQUEST*2)).thenReturn(secondPart);
        when(someBigCrumbList.subList(MAX_CRUMB_IN_ONE_REQUEST*2,MAX_CRUMB_IN_ONE_REQUEST*3-2)).thenReturn(thirdPart);
        doReturn(true).doReturn(false).doReturn(true).when(client).recordCrumbsInternal(any(List.class), eq(ENTITY_ID));

        assertFalse(client.recordCrumbs(someBigCrumbList,ENTITY_NAME));

        verify(client).recordCrumbsInternal(firstPart, ENTITY_ID);
        verify(client).recordCrumbsInternal(secondPart, ENTITY_ID);
        verify(client).recordCrumbsInternal(thirdPart, ENTITY_ID);
    }

    @Test
    public void shouldCheckIsEntityForCurrentDeviceExistBeforeSendRecordCrumbRequest() throws IOException {
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        doReturn(DEFAULT_ENTITY_GET_LIST_RESPONSE).doReturn("").when(client).sendRequest(captor.capture());
        doReturn(true).when(client).recordCrumbsInternal(any(List.class), eq(ENTITY_ID));

        client.recordCrumbs(Arrays.asList(crumb),ENTITY_NAME);
        assertEquals(ENTITY_GET_LIST_METHOD_URL,captor.getValue().getUrl());
    }

    @Test
    public void shouldNotCallCreateEntityIfEntityExist() throws IOException {
        doReturn(DEFAULT_ENTITY_GET_LIST_RESPONSE).when(client).sendRequest(any(HttpRequest.class));
        client.recordCrumbs(Arrays.asList(crumb), ENTITY_NAME);
        verify(client,never()).createEntities(any(List.class));
    }

    @Test
    public void shouldCallCreateEntityIfEntityNotExist() throws IOException {
        doReturn(DEFAULT_ENTITY_GET_LIST_RESPONSE).when(client).sendRequest(any(HttpRequest.class));
        doReturn(Arrays.asList(ENTITY_ID)).when(client).createEntities(Arrays.asList(OTHER_ENTITY_NAME));
        client.recordCrumbs(Arrays.asList(crumb), OTHER_ENTITY_NAME);
        verify(client).createEntities(Arrays.asList(OTHER_ENTITY_NAME));
    }




}