package com.gps.utils.store;

import android.content.Context;
import android.location.Location;
import com.google.gson.Gson;
import com.gps.utils.dto.Crumb;
import com.gps.utils.dto.RecordCrumbRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.io.*;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

public class JsonStorageTest {

    private Gson gson = new Gson();

    private static final String STORE_FILE_NAME = "crumbs";


    private static final String JSON_FOR_ONE_LOCATION_POINT = "{" +
            "\"crumbs\":[" +
            "{" +
            "\"location\":{" +
            "\"lat\":\"23.834\"," +
            "\"lng\":\"34.124\"" +
            "}," +
            "\"timestamp\":123456" +
            "}" +
            "]" +
            "}";

    private static final String JSON_FOR_TWO_CRUMBS = "{\n" +
            "\"crumbs\": [" +
            "{" +
            " \"location\": {" +
            "\"lat\": \"21.834\"," +
            "\"lng\": \"31.124\"" +
            "}," +
            "\"timestamp\": \"6783\"" +
            "}," +
            "{" +
            " \"location\": {" +
            " \"lat\":\"13.834\"," +
            " \"lng\":\"24.124\"" +
            "}," +
            "\"timestamp\":654321" +
            "}" +
            "]" +
            "}";
    private static final String JSON_FOR_THREE_CRUMBS = "{" +
            "\"crumbs\":[" +
            "{" +
            "\"location\":{" +
            "\"lat\":\"21.834\"," +
            "\"lng\":\"31.124\"" +
            "}," +
            "\"timestamp\":6783" +
            "}," +
            "{" +
            "\"location\":{" +
            "\"lat\":\"13.834\"," +
            "\"lng\":\"24.124\"" +
            "}," +
            "\"timestamp\":654321" +
            "}," +
            "{" +
            "\"location\":{" +
            "\"lat\":\"23.834\"," +
            "\"lng\":\"34.124\"" +
            "}," +
            "\"timestamp\":123456" +
            "}" +
            "]" +
            "}";

    private JsonStorage storage;

    private FileOutputStream fileOutputStream;

    private Location location;

    private Context context;


    @Before
    public void setUp() throws FileNotFoundException {
        fileOutputStream = mock(FileOutputStream.class);
        location = mock(Location.class);
        context = mock(Context.class);
        JsonStorage originalStorage = new JsonStorage(context);
        storage = spy(originalStorage);
        when(location.getLongitude()).thenReturn(34.124d);
        when(location.getLatitude()).thenReturn(23.834d);
        when(location.getTime()).thenReturn(123456000L);
        when(context.openFileOutput(STORE_FILE_NAME, Context.MODE_PRIVATE)).thenReturn(fileOutputStream);
    }

    @Test
    public void shouldStoreLocationToJson() throws IOException {
        doReturn("").when(storage).readCrumbsFromFile();
        storage.storeLocation(location);

        ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(fileOutputStream).write(captor.capture(), eq(0), anyInt());
        String result = new String(captor.getValue());
        assertEquals(JSON_FOR_ONE_LOCATION_POINT, result);
    }

    @Test
    public void shouldCloseStream() throws IOException {
        doReturn(JSON_FOR_TWO_CRUMBS).when(storage).readCrumbsFromFile();
        storage.storeLocation(location);
        verify(fileOutputStream).close();
    }

    @Test
    public void shouldDeleteStorageFile() {
        storage.clearStore();
        verify(context).deleteFile(STORE_FILE_NAME);
    }

    @Test
    public void shouldReturnAllCrumbsFromFile() throws IOException {
        doReturn(JSON_FOR_TWO_CRUMBS).when(storage).readCrumbsFromFile();
        List<Crumb> result = storage.getAllSavedLocations();
        List<Crumb> expected = gson.fromJson(JSON_FOR_TWO_CRUMBS, RecordCrumbRequest.class).getCrumbs();
        assertEquals(expected.get(0).getLocation().getLat(),result.get(0).getLocation().getLat());
        assertEquals(expected.get(1).getTimestamp(),result.get(1).getTimestamp());
    }

    @Test
    public void shouldAddNewCrumbToTheExistedCrumb() throws IOException {
        doReturn(JSON_FOR_TWO_CRUMBS).when(storage).readCrumbsFromFile();
        storage.storeLocation(location);
        ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(fileOutputStream).write(captor.capture(), eq(0), anyInt());
        String result = new String(captor.getValue());
        assertEquals(JSON_FOR_THREE_CRUMBS, result);
    }

}